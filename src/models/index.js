const Sequelize = require('sequelize');
const fixtures = require('sequelize-fixtures');

const { DATABASE_URL } = process.env;

// Initialize sequelize with different config based on environment
const sequelize = process.env.NODE_ENV === 'development'
  ? new Sequelize(DATABASE_URL, {
    dialect: 'postgres',
    operatorsAliases: false,
    logging: false
  })
  : new Sequelize(DATABASE_URL, {
    dialect: 'postgres',
    operatorsAliases: false,
    logging: false
  });

// Initialize models
const User = require('./user')(sequelize);

module.exports = {
  fixtures,
  sequelize,
  Sequelize,
  User
};
